name: 'gitlab_snowflake'
version: '1.0'

profile: 'gitlab-snowflake'

source-paths: ["models"]
test-paths: ["tests"]
data-paths: ["data"]
macro-paths: ["macros"]

target-path: "target"
clean-targets:
    - "target"
    - "dbt_modules"

quoting:
    database: true
    identifier: false
    schema: false

on-run-start:
    - "{{ dbt_logging_start('on run start hooks') }}"
    - "{{resume_warehouse(var('resume_warehouse', false), var('warehouse_name'))}}"
    - "{{create_udfs()}}"

on-run-end:
    - "{{ dbt_logging_start('on run end hooks') }}"
    - "{{grant_usage_to_schemas(schema_name, rolename)}}"
    - "{{suspend_warehouse(var('suspend_warehouse', false), var('warehouse_name'))}}"

models:
    pre-hook:
      - "{{ logging.log_model_start_event() }}"
    post-hook:
      - "{{ logging.log_model_end_event() }}"
    vars:
      database: 'raw'
      warehouse_name: "{{ env_var('SNOWFLAKE_TRANSFORM_WAREHOUSE') }}"

    logging:
      schema: meta
      post-hook:
        - "grant select on {{this}} to role reporter"

    snowplow:
      schema: analytics
      tags: ["product"]
      post-hook: "grant select on {{this}} to role reporter"
      vars:
        'snowplow:use_fivetran_interface': false
        'snowplow:events': "{{ref('snowplow_unnested_events')}}"
        'snowplow:context:web_page': "{{ref('snowplow_web_page')}}"
        'snowplow:context:performance_timing': false
        'snowplow:context:useragent': false
        'snowplow:timezone': 'America/New_York'
        'snowplow:page_ping_frequency': 30
        'snowplow:app_ids': ['gitlab', 'about']
        'snowplow:pass_through_columns': ['cf_formid','cf_elementid','cf_nodename','cf_type','cf_elementclasses','cf_value','sf_formid','sf_formclasses','sf_elements','ff_formid','ff_elementid','ff_nodename','ff_elementtype','ff_elementclasses','ff_value','lc_elementid','lc_elementclasses','lc_elementtarget','lc_targeturl','lc_elementcontent','tt_category','tt_variable','tt_timing','tt_label']

    snowflake_spend:
      enabled: true
      base:
        materialized: table
      xf:
        materialized: table

    gitlab_snowflake:
      enabled: true
      materialized: view

      bamboohr:
        schema: sensitive
        enabled: true
        base:
          materialized: table

      customers:
        base:
          enabled: true
          materialized: table

      date:
        base:
          enabled: true
          materialized: table

      dbt_logging:
        enabled: false

      gitlab_data_yaml:
        enabled: true
        materialized: table

      gitter:
        enabled: false
        transformed:
          materialized: table

      gitlab_dotcom:
        tags: ["product"]
        enabled: true
        xf:
          materialized: table

      netsuite:
        enabled: false

      netsuite_stitch:
        enabled: true
        base:
          materialized: table
        xf:
          materialized: table

      pings:
        tags: ["product"]
        enabled: true

      pipe2spend:
        enabled: false
        materialized: table

      retention:
        enabled: true
        materialized: table

      snowplow:
        tags: ["product"]
        enabled: true
        xf:
          materialized: table

      sfdc:
        xf:
          enabled: true
          materialized: table

      zuora:
        xf:
          enabled: true
          materialized: table

      zendesk:
        xf:
          enabled: true
          materialized: table
